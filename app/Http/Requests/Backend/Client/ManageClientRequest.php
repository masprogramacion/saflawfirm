<?php

namespace App\Http\Requests\Backend\Client;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ManageClientRequest.
 */
class ManageClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //dd('here1');
        //return true;
         //TODO this can be used by a regular user as well
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
