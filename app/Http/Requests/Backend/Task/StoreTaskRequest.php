<?php

namespace App\Http\Requests\Backend\Task;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;


/**
 * Class StoreTaskRequest.
 */
class StoreTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //dd('here1');
        //return true;
         //TODO this can be used by a regular user as well
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      // dd($this->request);
        $client_id = $this->request->get('client_id');
        $project_id = $this->request->get('project_id');

        return [
            'client_id' => 'required',
            'project_id' => ['required',
                    Rule::unique('tasks')->where(function ($query) use ($client_id, $project_id)  {
                        return $query->where('client_id', $client_id)
                        ->where('project_id', $project_id);
                    }),
            ],
            'bank_id' => 'required',
            'seller_id' => 'required',
        ];
    }
}
