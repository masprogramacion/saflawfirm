<?php

namespace App\Http\Requests\Backend\Task;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateTaskRequest.
 */
class UpdateTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //dd('here1');
        //return true;
         //TODO this can be used by a regular user as well
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'client_id' => 'required',
            'project_id' => 'required',
            'bank_id' => 'required',
            'seller_id' => 'required',
        ];

        foreach($this->request->all() as $key => $val)
        {
          if(starts_with($key, 'ev_date_') && $val !==null) {
             $rules[$key] = 'date';
          }
            
        }
        //dd($rules);
        return $rules;

    }
}
