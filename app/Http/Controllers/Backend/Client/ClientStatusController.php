<?php

namespace App\Http\Controllers\Backend\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Client\ManageClientRequest;
use App\Repositories\ClientRepositoryInterface;


class ClientStatusController extends Controller
{
    /** @var ClientRepositoryInterface */
    private $repository;

    public function __construct(ClientRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return @return mixed
     */
    public function show($id)
    {
        return $this->repository->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



        /**
     * @param ManageProductRequest $request
     * @param Product              $deletedProduct
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function delete(ManageClientRequest $request, Product $deletedProduct)
    {
        $this->productRepository->forceDelete($deletedProduct);

        return redirect()->route('admin.auth.product.deleted')->withFlashSuccess(__('alerts.backend.products.deleted_permanently'));
    }

    /**
     * @param ManageProductRequest $request
     * @param Product              $deletedProduct
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function restore(ManageProductRequest $request, Product $deletedProduct)
    {
        $this->productRepository->restore($deletedProduct);

        return redirect()->route('admin.auth.product.index')->withFlashSuccess(__('alerts.backend.products.restored'));
    }

}
