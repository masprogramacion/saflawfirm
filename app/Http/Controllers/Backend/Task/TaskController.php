<?php

namespace App\Http\Controllers\Backend\Task;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Task;
use App\Models\Bank;
use App\Models\Project;
use App\Models\User;
use App\Models\Client;
use App\Models\Event;
use App\Models\EventType;

use App\Http\Requests\Backend\Task\StoreTaskRequest;
use App\Http\Requests\Backend\Task\UpdateTaskRequest;

use Illuminate\Support\Facades\DB;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $events1 = DB::table('events')
        ->select('task_id', DB::raw('MAX(created_at) as ev1_created_at'))
        ->where('event_type_id', '1')
        ->groupBy('task_id');

        $events2 = DB::table('events')
        ->select('task_id', DB::raw('MAX(created_at) as ev2_created_at'))
        ->where('event_type_id', '2')
        ->groupBy('task_id');

        $events3 = DB::table('events')
        ->select('task_id', DB::raw('MAX(created_at) as ev3_created_at'))
        ->where('event_type_id', '3')
        ->groupBy('task_id');

        $events4 = DB::table('events')
        ->select('task_id', DB::raw('MAX(created_at) as ev4_created_at'))
        ->where('event_type_id', '4')
        ->groupBy('task_id');

        $events5 = DB::table('events')
        ->select('task_id', DB::raw('MAX(created_at) as ev5_created_at'))
        ->where('event_type_id', '5')
        ->groupBy('task_id');

        $events6 = DB::table('events')
        ->select('task_id', DB::raw('MAX(created_at) as ev6_created_at'))
        ->where('event_type_id', '6')
        ->groupBy('task_id');

        $events7 = DB::table('events')
        ->select('task_id', DB::raw('MAX(created_at) as ev7_created_at'))
        ->where('event_type_id', '7')
        ->groupBy('task_id');

        $events8 = DB::table('events')
        ->select('task_id', DB::raw('MAX(created_at) as ev8_created_at'))
        ->where('event_type_id', '8')
        ->groupBy('task_id');


        $events9 = DB::table('events')
        ->select('task_id', DB::raw('MAX(created_at) as ev9_created_at'))
        ->where('event_type_id', '9')
        ->groupBy('task_id');

        $events10 = DB::table('events')
        ->select('task_id', DB::raw('MAX(created_at) as ev10_created_at'))
        ->where('event_type_id', '10')
        ->groupBy('task_id');

        $events11 = DB::table('events')
        ->select('task_id', DB::raw('MAX(created_at) as ev11_created_at'))
        ->where('event_type_id', '11')
        ->groupBy('task_id');

        $client_name = DB::table('clients')
        ->select('id as client_id', DB::raw("CONCAT(first_name,' ',last_name) as client_name"));

        $project_name = DB::table('projects')
        ->select('id as project_id', DB::raw("name as project_name"));

        $seller_name = DB::table('users')
        ->select('id as user_id', DB::raw("CONCAT(first_name,' ',last_name) as seller_name"));

        $tasks = DB::table('tasks')
        ->leftJoinSub($events1, 'events1', function ($join) {
        $join->on('tasks.id', '=', 'events1.task_id');})
        ->leftJoinSub($events2, 'events2', function ($join) {
            $join->on('tasks.id', '=', 'events2.task_id');})
        ->leftJoinSub($events3, 'events3', function ($join) {
            $join->on('tasks.id', '=', 'events3.task_id');}) 
        ->leftJoinSub($events4, 'events4', function ($join) {
            $join->on('tasks.id', '=', 'events4.task_id');})
        ->leftJoinSub($events5, 'events5', function ($join) {
            $join->on('tasks.id', '=', 'events5.task_id');})
        ->leftJoinSub($events6, 'events6', function ($join) {
            $join->on('tasks.id', '=', 'events6.task_id');})
        ->leftJoinSub($events7, 'events7', function ($join) {
            $join->on('tasks.id', '=', 'events7.task_id');})
        ->leftJoinSub($events8, 'events8', function ($join) {
            $join->on('tasks.id', '=', 'events8.task_id');})
        ->leftJoinSub($events9, 'events9', function ($join) {
            $join->on('tasks.id', '=', 'events9.task_id');})
        ->leftJoinSub($events10, 'events10', function ($join) {
            $join->on('tasks.id', '=', 'events10.task_id');})
        ->leftJoinSub($events11, 'events11', function ($join) {
            $join->on('tasks.id', '=', 'events11.task_id');}) 
        ->leftJoinSub($client_name, 'client_name', function ($join) {
            $join->on('tasks.client_id', '=', 'client_name.client_id');})
        ->leftJoinSub($project_name, 'project_name', function ($join) {
            $join->on('tasks.project_id', '=', 'project_name.project_id');})   
        ->leftJoinSub($seller_name, 'seller_name', function ($join) {
            $join->on('tasks.seller_id', '=', 'seller_name.user_id');})                                                                                                                                
        ->get();

        // $tasks= DB::table('tasks') 
        //         ->leftjoin('events as ev1', 'ev1.task_id', '=', 'tasks.id')
        //         ->leftjoin('events as ev2', 'ev2.task_id', '=', 'tasks.id')
        //         ->select(DB::raw('tasks.client_id', 'tasks.project_id', 'tasks.seller_id', 'tasks.bank_id', 'ev1.created_at','ev2.created_at'))
        //         ->where('ev1.event_type_id' ,'1')
        //         ->where('ev2.event_type_id' ,'2')
        //         ->get();
        // $query = Worker::select('workers.name_and_surname', 'workers.id', 'workers.location','company_saved_cv.worker_id')
        // ->leftJoin('company_saved_cv', function($leftJoin)use($company_id)
        // {
        //     $leftJoin->on('workers.id', '=', 'company_saved_cv.worker_id');
        //     $leftJoin->on(DB::raw('company_saved_cv.company_id'), DB::raw('='),DB::raw("'".$company_id."'"));

        // DB::table('articles')
        // ->leftJoin('articles', 'articles.id', '=', 'articles.user_id')
        // ->leftJoin('news_groups', 'news_groups.id', '=', 'articles.news_group_id')
        // ->leftJoin('magazine_groups', 'magazine_groups.id', '=', 'articles.magazine_groups_id')
        // ->select(DB::raw('articles.id, type, title, articles.published_at, users.first_name, users.last_name, COALESCE(news_groups.name, magazine_groups.name) as group_name'))
        // ->groupBy('articles.id');


        // })
        //dd($tasks);
        return view('admin.task.index')
        ->with('tasks', $tasks);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $clients = Client::select(
            DB::raw("CONCAT(first_name,' ',last_name) AS name"),'id')
            ->pluck('name', 'id');
        $banks = Bank::pluck('name','id');
        $projects = Project::pluck('name','id');
        $sellers = User::role('vendedor')->pluck("first_name as name",'id'); 
        //dd($sellers);
        return view('admin.task.create')
        ->with('clients', $clients)
        ->with('banks', $banks)
        ->with('projects', $projects)
        ->with('sellers', $sellers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTaskRequest $request)
    {
        Task::create($request->all());
   
        return redirect()->route('admin.funcs.task.index')->withFlashSuccess('Tarea creada exitosamente.'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::findOrFail($id);
        $clients = Client::select(
            DB::raw("CONCAT(first_name,' ',last_name) AS name"),'id')
            ->pluck('name', 'id');
        $banks = Bank::pluck('name','id');
        $projects = Project::pluck('name','id');
        $sellers = User::role('vendedor')->pluck("first_name as name",'id'); 

        $event_types = EventType::all();
        $events = Event::where('task_id', '=',$task->id)
        ->get();
        //dd($sellers);
        return view('admin.task.edit')
        ->with('clients', $clients)
        ->with('banks', $banks)
        ->with('projects', $projects)
        ->with('sellers', $sellers)
        ->with('events', $events)
        ->with('event_types', $event_types)
        ->with('task', $task);
        

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTaskRequest $request, Task $task)
    {
 
        
        $task->update($request->all());

        foreach($request->all() as $key => $val)
        {
          if(starts_with($key, 'ev_date_') && $val !==null) {
             $event_type_id = str_replace("ev_date_","",$key);
             Event::updateOrCreate(['task_id' => $task->id, 'event_type_id'=>$event_type_id],
                        ['task_id' => $task->id, 'event_type_id'=>$event_type_id,'created_at' => $val]);
             // make your affectation to the $table1

          }
            
        }

  
        return redirect()->route('admin.funcs.task.index')
                        ->withFlashSuccess('Tarea actualizada exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
