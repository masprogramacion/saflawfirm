<?php

namespace App\Models\Traits\Relationship;

use App\Models\EventType;


/**
 * Class EventRelationship.
 */
trait EventRelationship
{
    
    /**
     * @return mixed
     */
    public function eventType()
    {
        return $this->hasOne(EventType::class);    
    }    
    



}
