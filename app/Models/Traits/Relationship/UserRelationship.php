<?php

namespace App\Models\Auth\Traits\Relationship;

use App\Models\System\Session;
use App\Models\Auth\SocialAccount;
use App\Models\Auth\PasswordHistory;
use App\Models\Auth\Product;
use App\Models\Auth\Profile;
use App\Models\Auth\BankAccount;
use App\Models\Auth\Message;
use App\Models\Auth\File;
/**
 * Class UserRelationship.
 */
trait UserRelationship
{
    /**
     * @return mixed
     */
    public function providers()
    {
        return $this->hasMany(SocialAccount::class,'user_id');
    }

    /**
     * @return mixed
     */
    public function sessions()
    {
        return $this->hasMany(Session::class);
    }

    /**
     * @return mixed
     */
    public function passwordHistories()
    {
        return $this->hasMany(PasswordHistory::class);
    }

    /**
     * @return mixed
     */
    public function products()
    {
        return $this->hasMany(Product::class);    
    }


    /**
     * @return mixed
     */
    public function profile()
    {
        return $this->hasOne(Profile::class);    
    }


    /**
     * @return mixed
     */
    public function bankAccount()
    {
        return $this->hasOne(BankAccount::class);    
    }

    // A user can send a message
    public function sent()
    {
        return $this->hasMany(Message::class, 'sender_id');
    }

    // A user can also receive a message
    public function received()
    {
        return $this->hasMany(Message::class, 'sent_to_id');
    }

    public function workflowStatus()
    {
        return $this->hasMany(WorkflowStatus::class, 'user_id');
    }


        /**
     * @return mixed
     */
    public function operationsDocument()
    {
        return $this->hasOne(File::class);    
    }
}
