<?php

namespace App\Models\Traits\Relationship;

use App\Models\Client;
use App\Models\Bank;
use App\Models\User;
use App\Models\Project;

/**
 * Class TaskRelationship.
 */
trait TaskRelationship
{
    
    /**
     * @return mixed
     */
    public function client()
    {
        return $this->hasOne(Client::class);    
    }    
    
     /**
     * @return mixed
     */
    public function project()
    {
        return $this->hasOne(Project::class);    
    }    
       
    /**
     * @return mixed
     */
    public function seller()
    {
        return $this->hasOne(User::class);    
    }      
    
    /**
     * @return mixed
     */
    public function bank()
    {
        return $this->hasOne(Bank::class);    
    } 


}
