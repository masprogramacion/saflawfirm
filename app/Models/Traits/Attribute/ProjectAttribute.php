<?php

namespace App\Models\Traits\Attribute;
/**
 * Trait ProjectAttribute.
 */
trait ProjectAttribute
{
    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        return '<a href="'.route('admin.funcs.project.edit', $this).'" name="edit_item" class="btn btn-info"><i class="fa fa-edit" data-toggle="tooltip" data-placement="top" title="Editar Proyecto"></i></a> ';
    }
}