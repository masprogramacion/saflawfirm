<?php

namespace App\Models\Traits\Attribute;
/**
 * Trait ClientAttribute.
 */
trait ClientAttribute
{
    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        return '<a href="'.route('admin.funcs.client.edit', $this).'" name="confirm_item" class="btn btn-info"><i class="fa fa-edit" data-toggle="tooltip" data-placement="top" title="Editar Cliente"></i></a> ';
    }
}