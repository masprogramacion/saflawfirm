<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use  App\Models\Traits\Relationship\EventTypeRelationship;

class EventType extends Model
{
    
    use EventTypeRelationship;
    //
}
