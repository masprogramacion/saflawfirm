<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use  App\Models\Traits\Relationship\EventRelationship;

class Event extends Model
{
    use EventRelationship;
    //
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'task_id', 'event_type_id','created_at',
    ];
}
