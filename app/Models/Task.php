<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\Attribute\TaskAttribute;
use App\Models\Traits\Method\TaskMethod;
use App\Models\Traits\Relationship\TaskRelationship;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use TaskRelationship,
    SoftDeletes;

        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id',
        'project_id',
        'bank_id',
        'seller_id',
    ];
}
