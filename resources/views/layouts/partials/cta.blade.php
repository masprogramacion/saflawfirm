<div class="cta-area section_gap overlay">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-8">
					<h1>Get to Know Project Estimate?</h1>
					<p>There is a moment in the life of any aspiring astronomer that it is time to buy that first telescope. It’s
						exciting to think about setting up your own viewing station whether that is on the deck</p>
					<a href="#" class="primary-btn">Get Free Estimate</a>
				</div>
			</div>
		</div>
	</div>