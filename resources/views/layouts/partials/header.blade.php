<header class="header_area">
		<div class="main_menu">
			<nav class="navbar navbar-expand-lg navbar-light">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<a class="navbar-brand logo_h" href="{{ route('frontend.index') }}"><img src="img/logo1.png" alt=""></a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
					 aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
						<ul class="nav navbar-nav menu_nav ml-auto">
							<li class="nav-item {{ active_class(Active::checkUriPattern('frontend.index')) }}"><a class="nav-link" href="{{ route('frontend.index') }}">Inicio</a></li>
							<li class="nav-item {{ active_class(Active::checkUriPattern('frontend.about-us')) }}"><a class="nav-link" href="{{ route('frontend.about-us') }}">Nosotros</a></li>
							<li class="nav-item {{ active_class(Active::checkUriPattern('frontend.services')) }}"><a class="nav-link" href="{{ route('frontend.services') }}">Servicios</a></li>
							<li class="nav-item {{ active_class(Active::checkUriPattern('frontend.real-estate')) }}"><a class="nav-link" href="{{ route('frontend.real-estate') }}">Bienes Raices</a></li>
							<!--
							<li class="nav-item submenu dropdown">
								<a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
								 aria-expanded="false">Pages</a>
								<ul class="dropdown-menu">
									<li class="nav-item"><a class="nav-link" href="elements.html">Elements</a></li>
								</ul>
							</li>
							<li class="nav-item submenu dropdown">
								<a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
								 aria-expanded="false">Blog</a>
								<ul class="dropdown-menu">
									<li class="nav-item"><a class="nav-link" href="blog.html">Blog</a></li>
									<li class="nav-item"><a class="nav-link" href="single-blog.html">Blog Details</a></li>
								</ul>
							</li>
							-->
							<li class="nav-item"><a class="nav-link" href="{{ route('frontend.contact') }}">Contactenos</a></li>
						</ul>
						<ul class="nav navbar-nav ml-auto">
							<div class="social-icons d-flex align-items-center">
								<a href="">
									<li><i class="fa fa-facebook"></i></li>
								</a>
								<a href="">
									<li><i class="fa fa-twitter"></i></li>
								</a>
							</div>
							<li class="nav-item"><a href="{{ route('login') }}" class="nav-link">
								<i class="lnr lnr-enter" id="promotores"></i> Promotores</a>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</div>
	</header>