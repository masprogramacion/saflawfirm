<section class="features_area" id="features_counter">
		<div class="container">
			<div class="row counter_wrapper">
				<!-- single feature -->
				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="single_feature">
						<div class="thumb">
							<img src="img/features/1.png" alt="">
						</div>
						<div class="info-content">
							<h4><span class="counter">596</span>+</h4>
							<p>Qualified Lawyer</p>

						</div>
					</div>
				</div>
				<!-- single feature -->
				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="single_feature">
						<div class="thumb">
							<img src="img/features/2.png" alt="">
						</div>
						<div class="info-content">
							<h4><span class="counter">20650</span>+</h4>
							<p>Solved Cases</p>
						</div>
					</div>
				</div>
				<!-- single feature -->
				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="single_feature">
						<div class="thumb">
							<img src="img/features/3.png" alt="">
						</div>
						<div class="info-content">
							<h4><span class="counter">2.5</span>k</h4>
							<p>Trusted Clients</p>
						</div>
					</div>
				</div>
				<!-- single feature -->
				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="single_feature">
						<div class="thumb">
							<img src="img/features/4.png" alt="">
						</div>
						<div class="info-content">
							<h4><span class="counter">50</span>+</h4>
							<p>Achievements</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>