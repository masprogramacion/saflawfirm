<section class="section_gap team-area">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-7">
					<div class="main_title">
						<h2>Meet Our Experienced Team</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed eiusmod tempor incididunt labore dolore magna
							aliqua enim minim veniam quis nostrud.</p>
					</div>
				</div>
			</div>
			<div class="row justify-content-center">
				<!-- single-team-member -->
				<div class="col-lg-4 col-md-4 col-sm-6">
					<div class="single_member">
						<div class="author">
							<img class="img-fluid" src="img/team/team1.jpg" alt="">
						</div>
						<div class="author_decs">
							<h5>Ethel Davis</h5>
							<p class="profession">Senior Barrister at law</p>
						</div>
					</div>
				</div>
				<!-- single-team-member -->
				<div class="col-lg-4 col-md-4 col-sm-6">
					<div class="single_member">
						<div class="author">
							<img class="img-fluid" src="img/team/team2.jpg" alt="">
						</div>
						<div class="author_decs">
							<h5>Ethel Davis</h5>
							<p class="profession">Senior Barrister at law</p>
						</div>
					</div>
				</div>
				<!-- single-team-member -->
				<div class="col-lg-4 col-md-4 col-sm-6">
					<div class="single_member">
						<div class="author">
							<img class="img-fluid" src="img/team/team3.jpg" alt="">
						</div>
						<div class="author_decs">
							<h5>Ethel Davis</h5>
							<p class="profession">Senior Barrister at law</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>