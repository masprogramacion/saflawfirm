<section class="service-area-2">
		<div class="container">
			<div class="row align-items-center justify-content-center">
				<div class="col-lg-6">
					<div class="service-2-left">
						<div class="get-know">
							<p class="df-color">Get to Know Project Estimate?</p>
							<h1>Get to Know Project Estimate?</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed eiusmod tempor incididunt labore dolore magna
								aliqua enim minim veniam quis nostrud.</p>
						</div>
						<div class="author-lacture">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed eiusmod tempor incididunt labore dolore magna
								aliqua enim minim veniam quis nostrud. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed eiusmod
								tempor incididunt labore dolore magna aliqua enim minim veniam quis nostrud. Lorem ipsum dolor sit amet,
								consectetur adipisicing elit, sed eiusmod tempor incididunt labore dolore magna aliqua enim minim veniam quis
								nostrud.</p>
							<div class="author-title">
								<div class="thumb"><img src="img/about-author.png" alt=""></div>
								<div class="a-desc">
									<h6>Marvel Maison</h6>
									<p>Chief Executive, Amazon</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="service-2-right">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="left-image">
									<div class="s-img"><img class="img-fluid" src="img/service/service4.jpg" alt=""></div>
									<div class="s-img"><img class="img-fluid" src="img/service/service5.jpg" alt=""></div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="right-image">
										<div class="s-img"><img class="img-fluid" src="img/service/service6.jpg" alt=""></div>
										<div class="s-img"><img class="img-fluid" src="img/service/service7.jpg" alt=""></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>