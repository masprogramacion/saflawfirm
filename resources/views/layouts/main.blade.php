<!doctype html>
<html lang="en">

<head>
@include('layouts.partials.head')
@stack('stylesheets')
</head>

<body>

	<!--================ Start Header Menu Area =================-->
	@include('layouts.partials.header')
	<!--================ End Header Menu Area =================-->
	@yield('content')

	<!--================  start footer Area =================-->
	@include('layouts.partials.footer')
	<!--================ End footer Area =================-->
	@include('layouts.partials.footer-scripts')
</body>

</html>