@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
      <h1>
				Configuracion del Sistema
        <small>Vista Administrativa</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">General Elements</li>
      </ol>
@endsection

@section('content')
<div class="row">
<div class="col-md-6">
<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Quick Example</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {{ html()->modelForm($client, 'PATCH', route('admin.funcs.client.update', $client->id))->attributes(['id'=>'form-update'])->open() }}

              <div class="box-body">
                <div class="form-group">
                  <label for="first_name">Nombre</label>
                  {{ html()->input('text','first_name')
                                        ->class('form-control')
                                        ->value($client->first_name) }}
                </div>
                <div class="form-group">
                  <label for="last_name">Apellido</label>
                  {{ html()->input('text','last_name')
                                        ->class('form-control')
                                        ->value($client->last_name) }}
                </div>

                <div class="form-group">
                  <label for="email">Email</label>
                  {{ html()->input('text','email')
                                        ->class('form-control')
                                        ->value($client->email) }}
                </div>

              </div><!-- /.box-body -->
              <div class="box-footer">
                {{ form_cancel(route('admin.funcs.client.index'), 'Cancelar') }}
                <button type="submit" class="btn btn-success pull-right">Actualizar</button>
              </div><!-- /.box-footer -->
            {{ html()->closeModelForm() }}
          </div>
          </div>
          </div>
@endsection