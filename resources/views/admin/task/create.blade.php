@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
      <h1>
				Configuracion del Sistema
        <small>Vista Administrativa</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">General Elements</li>
      </ol>
@endsection

@section('content')
<div class="row">
<div class="col-md-6">
<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Crear Tarea</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {{ html()->form('POST', route('admin.funcs.task.store'))->attributes(['id'=>'form-create'])->open() }}

              <div class="box-body">
                <div class="form-group">
                  <label for="client_id">Cliente</label>
                  {{ html()->select('client_id',$clients)
                              ->class('form-control')
                              ->placeholder('Seleccione una opcion')
                              ->required() }}
                </div>
                <div class="form-group">
                <label for="project_id">Proyecto</label>
                  {{ html()->select('project_id',$projects)
                              ->class('form-control')
                              ->placeholder('Seleccione una opcion')
                              ->required() }}
                </div>

                <div class="form-group">
                <label for="bank_id">Banco</label>
                  {{ html()->select('bank_id',$banks)
                              ->class('form-control')
                              ->placeholder('Seleccione una opcion')
                              ->required() }}
                </div>
                </div>
                <div class="form-group">
                <label for="seller_id">Vendedor</label>
                  {{ html()->select('seller_id',$sellers)
                              ->class('form-control')
                              ->placeholder('Seleccione una opcion')
                              ->required() }}
                </div>
              </div><!-- /.box-body -->
              <div class="box-footer">
                {{ form_cancel(route('admin.funcs.task.index'), 'Cancelar') }}
                <button type="submit" class="btn btn-success pull-right">Crear</button>
              </div><!-- /.box-footer -->
            {{ html()->closeModelForm() }}
          </div>
          </div>
          </div>
@endsection