@extends('adminlte::page')

@section('title', 'AdminLTE')
@section('css')
<link rel="stylesheet" href="{{ asset('vendors/jquery-ui/jquery-ui.css') }}">
<link rel="stylesheet" href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker3.min.css') }}">
@endsection
@section('content_header')
      <h1>
				Configuracion del Sistema
        <small>Vista Administrativa</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">General Elements</li>
      </ol>
@endsection

@section('content')
<div class="row">
<div class="col-md-6">
<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Editar Tarea</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {{ html()->modelForm($task, 'PATCH', route('admin.funcs.task.update', $task->id))->attributes(['id'=>'form-update'])->open() }}

            <div class="box-body">
                <div class="form-group">
                  <label for="client_id">Cliente</label>
                  {{ html()->select('client_id',$clients)
                              ->class('form-control')
                              ->value($task->client_id)
                              ->placeholder('Seleccione una opcion')
                              ->required() }}
                </div>
                <div class="form-group">
                <label for="project_id">Proyecto</label>
                  {{ html()->select('project_id',$projects)
                              ->class('form-control')
                              ->value($task->project_id)
                              ->placeholder('Seleccione una opcion')
                              ->required() }}
                </div>

                <div class="form-group">
                <label for="bank_id">Banco</label>
                  {{ html()->select('bank_id',$banks)
                              ->class('form-control')
                              ->value($task->bank_id)
                              ->placeholder('Seleccione una opcion')
                              ->required() }}
                </div>
                <div class="form-group">
                <label for="seller_id">Vendedor</label>
                  {{ html()->select('seller_id',$sellers)
                              ->class('form-control')
                              ->value($task->seller_id)
                              ->placeholder('Seleccione una opcion')
                              ->required() }}
                </div>
                @foreach($event_types as $event_type)
                <div class="form-group">
                <label for="seller_id">{{$event_type->name}}</label>
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <?php $created_at = ''; ?>
                    @foreach($events as $event)
                    
                      @if($event_type->id == $event->event_type_id)
                        <?php $created_at = $event->created_at->format('Y-m-d'); ?>
                      @endif

                    @endforeach
                    <input type="text" class="form-control datepickers" name="ev_date_{{$event_type->id}}" id="ev_date_{{$event_type->id}}" value="{{$created_at}}" >
                  </div>
                </div>
                @endforeach
              </div><!-- /.box-body -->
              <div class="box-footer">
                {{ form_cancel(route('admin.funcs.task.index'), 'Cancelar') }}
                <button type="submit" class="btn btn-success pull-right">Actualizar</button>
              </div><!-- /.box-footer -->
            {{ html()->closeModelForm() }}
          </div>
          </div>
          </div>
@endsection
@section('js')
<script src="{{ asset('vendors/jquery-ui/jquery-ui.js') }}"></script>
<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js') }}"></script>
<script>
  $( document ).ready(function() {
      
    $('.datepickers').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd',
        });

  });

</script>
@stop