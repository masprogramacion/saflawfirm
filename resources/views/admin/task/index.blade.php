@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
      <h1>
				Configuracion del Sistema
        <small>Vista Administrativa</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">General Elements</li>
      </ol>
@endsection

@section('content')

<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Seguimiento de Clientes</h3>
              <div class="btn-toolbar pull-right" role="toolbar" aria-label="Crear Nuevo Proyecto">
						<a href="{{ route('admin.funcs.task.create') }}" class="btn btn-success ml-1" data-toggle="tooltip" title="Crear Nueva Tarea"><i class="fa fa-fw fa-plus-circle"></i> Crear Nueva Tarea</a>
				</div><!--btn-toolbar-->
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
            @if($tasks)
              <table class="table table-hover">
                <tbody><tr>
                  <th></th>
                  <th>Cliente</th>
                  <th>Proyecto</th>
                  <th>Vendedor</th>
                  <th>Exp. Recibido</th>
                  <th>Minutas Promotora</th>
                  <th>Minutas Banco</th>
                  <th>Prot. Cliente</th>
                  <th>Multitrust</th>
                  <th>Protocolo Blanco</th>
                  <th>Escritura Cierre</th>
                  <th>Pres. Reg.Publico</th>
                  <th>Inscrip. Escritura</th>
                  <th>Entrega Escritura</th>
                  <th>Liquidado</th>
                </tr>
                @foreach($tasks as $task)
                <tr>
                  <td><a href="{{route('admin.funcs.task.edit', $task->id)}}" name="editar_item" class="btn btn-info"><i class="fa fa-edit" data-toggle="tooltip" data-placement="top" title="Editar Tarea"></i></a></td>
                  <td>{{$task->client_name}}</td>
                  <td>{{$task->project_name}}</td>
                  <td>{{$task->seller_name}}</td>
                  <td><span class="label label-success">{{$task->ev1_created_at ? \Carbon\Carbon::parse($task->ev1_created_at)->format('Y-m-d h:m'):''}}</span></td>
                  <td><span class="label label-warning">{{$task->ev2_created_at ? \Carbon\Carbon::parse($task->ev2_created_at)->format('Y-m-d h:m'):''}}</span></td>
                  <td><span class="label label-warning">{{$task->ev3_created_at ? \Carbon\Carbon::parse($task->ev3_created_at)->format('Y-m-d h:m'):''}}</span></td>
                  <td><span class="label label-success">{{$task->ev4_created_at ? \Carbon\Carbon::parse($task->ev4_created_at)->format('Y-m-d h:m'):''}}</span></td>
                  <td><span class="label label-warning">{{$task->ev5_created_at ? \Carbon\Carbon::parse($task->ev5_created_at)->format('Y-m-d h:m'):''}}</span></td>
                  <td><span class="label label-warning">{{$task->ev6_created_at ? \Carbon\Carbon::parse($task->ev6_created_at)->format('Y-m-d h:m'):''}}</span></td>
                  <td><span class="label label-success">{{$task->ev7_created_at ? \Carbon\Carbon::parse($task->ev7_created_at)->format('Y-m-d h:m'):''}}</span></td>
                  <td><span class="label label-warning">{{$task->ev8_created_at ? \Carbon\Carbon::parse($task->ev8_created_at)->format('Y-m-d h:m'):''}}</span></td>
                  <td><span class="label label-warning">{{$task->ev9_created_at ? \Carbon\Carbon::parse($task->ev9_created_at)->format('Y-m-d h:m'):''}}</span></td>
                  <td><span class="label label-success">{{$task->ev10_created_at ? \Carbon\Carbon::parse($task->ev10_created_at)->format('Y-m-d h:m'):''}}</span></td>
                  <td><span class="label label-warning">{{$task->ev11_created_at ? \Carbon\Carbon::parse($task->ev11_created_at)->format('Y-m-d h:m'):''}}</span></td>
                </tr>
                @endforeach
              </tbody></table>
              @endif
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>



@endsection
