@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
      <h1>
        Configuracion del Sistema
        <small>Vista Administrativa</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">General Elements</li>
      </ol>
@endsection

@section('content')
<!--
	<div class="row no-print">
        <div class="col-xs-12">
          <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
          <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
          </button>
          <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-download"></i> Generate PDF
          </button>
        </div>
  </div>
	-->
	<div class="">
		<div class="box">
			<div class="box-header">

				<h3 class="box-title">Proyectos</h3>

				<div class="btn-toolbar pull-right" role="toolbar" aria-label="Crear Nuevo Proyecto">
						<a href="{{ route('admin.funcs.project.create') }}" class="btn btn-success ml-1" data-toggle="tooltip" title="Crear Nuevo Proyecto"><i class="fa fa-fw fa-plus-circle"></i> Crear Nuevo Proyecto</a>
				</div><!--btn-toolbar-->

			</div>

			<div class="box-body">
				<table class="table table-responsive">
					<thead>
						<tr>
							<th>Nombre</th>
							<th>Acciones</th>
						</tr>
						
					</thead>

					<tbody>

						@foreach($projects as $project)
							<tr>
								<td>{{$project->name}}</td>
								<td>
									{!! $project->editButton !!}
									<button class="btn btn-danger" data-projectid={{$project->id}} data-toggle="modal" data-target="#delete"><span class="fa fa-trash-o fa-lg" aria-hidden="true"></span></button>
								</td>
							</tr>

						@endforeach
					</tbody>


				</table>				
			</div>
		</div>
	</div>



<!-- Modal -->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content panel-danger">
      <div class="modal-header panel-heading">
        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="myModalLabel">Confirmar Borrado</h4>
      </div>
      <form action="{{route('admin.funcs.project.delete')}}" method="post">
      		{{method_field('delete')}}
      		{{csrf_field()}}
	      <div class="modal-body">
				<p class="text-center">
					Ud esta seguro que desea borrar esto?
				</p>
	      		<input type="hidden" name="project_id" id="project_id" value="">

	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-success" data-dismiss="modal">No, Cancelar</button>
	        <button type="submit" class="btn btn-warning">Si, Borrar</button>
	      </div>
      </form>
    </div>
  </div>
</div>


@endsection

@section('js')
<script>


  $('#delete').on('show.bs.modal', function (event) {

      var button = $(event.relatedTarget) 

      var project_id = button.data('projectid') 
			//alert(project_id);
      var modal = $(this)

      modal.find('.modal-body #project_id').val(project_id);
})


</script>
@endsection


