@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
      <h1>
				Configuracion del Sistema
        <small>Vista Administrativa</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">General Elements</li>
      </ol>
@endsection

@section('content')
<div class="row">
<div class="col-md-6">
<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Editar Proyecto</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {{ html()->modelForm($project, 'PATCH', route('admin.funcs.project.update', $project->id))->class('needs-validation')->attributes(['id'=>'form-update'])->open() }}

              <div class="box-body">
                <div class="form-group">
                  <label for="first_name">Nombre</label>
                  {{ html()->input('text','name')
                                        ->class('form-control') }}
                </div>

              </div><!-- /.box-body -->
              <div class="box-footer">
                {{ form_cancel(route('admin.funcs.project.index'), 'Cancelar') }}
                <button type="submit" class="btn btn-success pull-right">Actualizar</button>
              </div><!-- /.box-footer -->
            {{ html()->closeModelForm() }}
          </div>
          </div>
          </div>
@endsection