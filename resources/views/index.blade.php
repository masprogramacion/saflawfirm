@extends('layouts.main')

@section('content')

	<!--================ Start Home Banner Area =================-->
	@include('layouts.partials.home-banner')
	<!--================ End Home Banner Area =================-->

	<!--================ Start Video Area =================-->
	@include('layouts.partials.video')
	<!--================ End video Area =================-->

	<!--================ Start service Area =================-->
	@include('layouts.partials.services')
	<!--================ End service Area =================-->

	<!--================ Start service-2 Area =================-->
	@include('layouts.partials.services2')
	<!--================ End service-2 Area =================-->


	<!--================ Start Team Area =================-->
	@include('layouts.partials.team')
	<!--================ End Team Area =================-->

	<!--================ Start CTA Area =================-->
	@include('layouts.partials.cta')
	<!--================ End CTA Area =================-->

	<!--================ Start Blog Area =================-->
	@include('layouts.partials.blog')
	<!--================ End Blog Area =================-->

@endsection


<b></b>