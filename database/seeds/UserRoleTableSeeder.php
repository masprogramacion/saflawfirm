<?php

use App\Models\User;
use Illuminate\Database\Seeder;

/**
 * Class UserRoleTableSeeder.
 */
class UserRoleTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

        User::find(1)->assignRole(config('access.users.admin_role'));
        User::find(2)->assignRole('executive');
        User::find(3)->assignRole(config('access.users.default_role'));
        User::find(4)->assignRole('vendedor');
        User::find(5)->assignRole('vendedor');
        $this->enableForeignKeys();
    }
}
