<?php

use Illuminate\Database\Seeder;

class BankTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys();
  

        DB::table('banks')->insert(['name' => 'ALLBANK']);
        DB::table('banks')->insert(['name' => 'ATLAS BANK']);
        DB::table('banks')->insert(['name' => 'BAC INTERNATIONAL BANK']);
        DB::table('banks')->insert(['name' => 'BANCO ALIADO']);
        DB::table('banks')->insert(['name' => 'BANCO AZTECA']);
        DB::table('banks')->insert(['name' => 'BANCO CANAL BANK']);
        DB::table('banks')->insert(['name' => 'BANCO DEL PACIFICO PANAMA S.A.']);
        DB::table('banks')->insert(['name' => 'BANCO DELTA']);
        DB::table('banks')->insert(['name' => 'BANCO FICOHSA PANAMA']);
        DB::table('banks')->insert(['name' => 'BANCO GENERAL']);
        DB::table('banks')->insert(['name' => 'BANCO INTERNACIONAL DE COSTA RICA - BICSA']);
        DB::table('banks')->insert(['name' => 'BANCO LAFISE PANAMA S.A']);
        DB::table('banks')->insert(['name' => 'BANCO NACIONAL']);
        DB::table('banks')->insert(['name' => 'BANCO PANAMA']);
        DB::table('banks')->insert(['name' => 'BANCO PICHINCHA PANAMA']);
        DB::table('banks')->insert(['name' => 'BANCOLOMBIA']);
        DB::table('banks')->insert(['name' => 'BANESCO']);
        DB::table('banks')->insert(['name' => 'BANISI S.A.']);
        DB::table('banks')->insert(['name' => 'BANISTMO S.A.']);
        DB::table('banks')->insert(['name' => 'BANVIVIENDA']);
        DB::table('banks')->insert(['name' => 'BBP BANK S.A.']);
        DB::table('banks')->insert(['name' => 'BCT BANK']);
        DB::table('banks')->insert(['name' => 'CACECHI']);
        DB::table('banks')->insert(['name' => 'CAJA DE AHORROS']);
        DB::table('banks')->insert(['name' => 'CAPITAL BANK']);
        DB::table('banks')->insert(['name' => 'CITIBANK N A.']);
        DB::table('banks')->insert(['name' => 'COEDUCO']);
        DB::table('banks')->insert(['name' => 'COOESAN']);
        DB::table('banks')->insert(['name' => 'COOPEDUC']);
        DB::table('banks')->insert(['name' => 'COOPERATIVA CRISTOBAL']);
        DB::table('banks')->insert(['name' => 'COOPERATIVA EDIOACC']);
        DB::table('banks')->insert(['name' => 'COOPERATIVA PROFESIONALES R.L.']);
        DB::table('banks')->insert(['name' => 'COOPEVE']);
        DB::table('banks')->insert(['name' => 'COOPRAC R.L.']);
        DB::table('banks')->insert(['name' => 'CREDICORP BANK']);
        DB::table('banks')->insert(['name' => 'DAVIVIENDA']);
        DB::table('banks')->insert(['name' => 'ECASESO']);
        DB::table('banks')->insert(['name' => 'G Y T CONTINENTAL PANAMA S.A.']);
        DB::table('banks')->insert(['name' => 'GLOBAL BANK']);
        DB::table('banks')->insert(['name' => 'MERCANTIL BANCO']);
        DB::table('banks')->insert(['name' => 'METROBANK']);
        DB::table('banks')->insert(['name' => 'MMG BANK']);
        DB::table('banks')->insert(['name' => 'MULTIBANK']);
        DB::table('banks')->insert(['name' => 'PRIVAL BANK']);
        DB::table('banks')->insert(['name' => 'ST. GEORGES BANK']);
        DB::table('banks')->insert(['name' => 'THE BANK OF NOVA SCOTIA']);
        DB::table('banks')->insert(['name' => 'TOWERBANK INT.']);
        DB::table('banks')->insert(['name' => 'UNIBANK']);

        
        $this->enableForeignKeys();
    }
}

