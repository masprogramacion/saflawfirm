<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class TaskTableSeeder extends Seeder
{
    use DisableForeignKeys;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $this->disableForeignKeys();

        DB::table('tasks')->insert(['client_id' => '1', 'project_id' => '1', 'seller_id' => '1', 'bank_id' => '1', 'created_at' => Carbon::now()]);


        $this->enableForeignKeys();
    }
}
