<?php

use Illuminate\Database\Seeder;

class ProjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('projects')->insert(['name' => 'Altos del Golf']);
        DB::table('projects')->insert(['name' => 'PH Oceania']);
        DB::table('projects')->insert(['name' => 'PH Vivendi']);
        DB::table('projects')->insert(['name' => 'PH Punta del Sol']);
        DB::table('projects')->insert(['name' => 'PH Crystal Palace']);
        DB::table('projects')->insert(['name' => 'PH Ocean Two']);
    }
}
