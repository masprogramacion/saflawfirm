<?php

use App\Models\User;
use Illuminate\Database\Seeder;

/**
 * Class UserTableSeeder.
 */
class UserTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

        User::truncate();

        // Add the master administrator, user id of 1
        User::create([
            'first_name'        => 'Admin',
            'last_name'         => 'Istrator',
            'email'             => 'admin@admin.com',
            'password'          => 'secret',
        ]);

        User::create([
            'first_name'        => 'Backend',
            'last_name'         => 'User',
            'email'             => 'executive@executive.com',
            'password'          => 'secret',

        ]);

        User::create([
            'first_name'        => 'Default',
            'last_name'         => 'User',
            'email'             => 'user@user.com',
            'password'          => 'secret',
        ]);

        User::create([
            'first_name'        => 'Juan',
            'last_name'         => 'Perez',
            'email'             => 'juan.perez@gmail.com',
            'password'          => 'secret',
        ]);


        User::create([
            'first_name'        => 'Eduardo',
            'last_name'         => 'Tejada',
            'email'             => 'eduardo.tejada@gmail.com',
            'password'          => 'secret',
        ]);


        $this->enableForeignKeys();
    }
}
