<?php

use Illuminate\Database\Seeder;

class ClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //\App\Models\Dog::truncate();

        (new Faker\Generator)->seed(123); //with this line, the seeder will create the exact same list of clients
    
        factory(App\Models\Client::class, 50)->create();

    }
}
