<?php

use Illuminate\Database\Seeder;

class EventTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('event_types')->insert(['name' => 'Exp. Recibido']);
        DB::table('event_types')->insert(['name' => 'Minutas Promotora']);
        DB::table('event_types')->insert(['name' => 'Minutas Banco']);
        DB::table('event_types')->insert(['name' => 'Prot. Cliente']);
        DB::table('event_types')->insert(['name' => 'Multitrust']);
        DB::table('event_types')->insert(['name' => 'Protocolo Blanco']);
        DB::table('event_types')->insert(['name' => 'Escritura Cierre']);
        DB::table('event_types')->insert(['name' => 'Pres. Reg.Publico']);
        DB::table('event_types')->insert(['name' => 'Inscrip. Escritura']);
        DB::table('event_types')->insert(['name' => 'Entrega Escritura']);
        DB::table('event_types')->insert(['name' => 'Liquidado']);
    }
}
