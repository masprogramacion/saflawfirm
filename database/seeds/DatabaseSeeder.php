<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    use TruncateTable;

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {


        Model::unguard();
        $this->call(UserTableSeeder::class);
        $this->call(PermissionRoleTableSeeder::class);
        $this->call(UserRoleTableSeeder::class);
        // $this->truncateMultiple([
        //     'banks',
        //     'event_types',
        //     'projects',
        //     'tasks',
        //     'clients',
        // ]);

       // $this->call(AuthTableSeeder::class);
        $this->call(BankTableSeeder::class);
        $this->call(ClientTableSeeder::class);
        $this->call(EventTypeTableSeeder::class);
        $this->call(ProjectTableSeeder::class);
        $this->call(TaskTableSeeder::class);
        

        Model::reguard();




 

    }
}
