<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->unsigned()->nullable();
            $table->integer('project_id')->unsigned()->nullable();
            $table->integer('seller_id')->unsigned()->nullable();
            $table->integer('bank_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['client_id', 'project_id']);

            $table->foreign('project_id')
            ->references('id')
            ->on('projects')->onUpdate('cascade');
    
            $table->foreign('seller_id')
            ->references('id')
            ->on('users')->onUpdate('cascade');
    
            $table->foreign('client_id')
            ->references('id')
            ->on('clients')->onDelete('cascade')->onUpdate('cascade');
    
            $table->foreign('bank_id')
            ->references('id')
            ->on('banks')->onUpdate('cascade');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
