<?php

use App\Http\Controllers\Frontend\FrontendController;


Route::get('/', [FrontendController::class, 'index'])->name('index');
Route::get('/services', [FrontendController::class, 'services'])->name('services');
Route::get('/about-us', [FrontendController::class, 'about_us'])->name('about-us');
Route::get('/real-estate', [FrontendController::class, 'real_estate'])->name('real-estate');
Route::get('/contact', [FrontendController::class, 'contact'])->name('contact');
