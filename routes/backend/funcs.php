<?php

use App\Http\Controllers\Backend\User\UserStatusController;
use App\Http\Controllers\Backend\User\UserController;
use App\Http\Controllers\Backend\Client\ClientController;
use App\Http\Controllers\Backend\Task\TaskController;
use App\Http\Controllers\Backend\Project\ProjectController;
use App\Http\Controllers\AdminController;

/*
 * All route names are prefixed with 'admin.funcs'.
 */


Route::get('home', [AdminController::class, 'index'])->name('home');


Route::group([
    'prefix'     => 'funcs',
    'as'         => 'funcs.',
    'namespace'  => 'Funcs',
    'middleware' => 'role:administrador',
], function () {
    /*
     * User Management
     */
    Route::group(['namespace' => 'User'], function () {

        /*
         * User Status'
         */
        Route::get('user/deactivated', [UserStatusController::class, 'getDeactivated'])->name('user.deactivated');
        Route::get('user/deleted', [UserStatusController::class, 'getDeleted'])->name('user.deleted');

        /*
         * User (only for System Type) CRUD
         */
        Route::get('user', [UserController::class, 'index'])->name('user.index');
        Route::get('user/create', [UserController::class, 'create'])->name('user.create');
        Route::post('user', [UserController::class, 'store'])->name('user.store');
    });


    /*
     * Client Management
     */
    Route::group(['namespace' => 'Client'], function () {


        /*
         * Client (only for System Type) CRUD
         */
        Route::get('client', [ClientController::class, 'index'])->name('client.index');
        Route::get('client/create', [ClientController::class, 'create'])->name('client.create');
        Route::post('client', [ClientController::class, 'store'])->name('client.store');
        Route::delete('client/delete', [ClientController::class, 'destroy'])->name('client.delete');


        Route::group(['prefix' => 'client/{client}'], function () {
            Route::get('/', [ClientController::class, 'show'])->name('client.show');
            Route::get('edit', [ClientController::class, 'edit'])->name('client.edit');

            Route::delete('/', [ClientController::class, 'destroy'])->name('client.destroy');
            Route::patch('update/', [ClientController::class, 'update'])->name('client.update');

            // Delete
            //Route::get('delete', [ClientStatusController::class, 'delete'])->name('client.delete-permanently');
            // Restore
            //Route::get('restore', [ClientStatusController::class, 'restore'])->name('client.restore');

        });



    });

    /*
     * Project Management
     */
    Route::group(['namespace' => 'Project'], function () {


        /*
         * Project (only for System Type) CRUD
         */
        Route::get('project', [ProjectController::class, 'index'])->name('project.index');
        Route::get('project/create', [ProjectController::class, 'create'])->name('project.create');
        Route::post('project', [ProjectController::class, 'store'])->name('project.store');
        Route::delete('project/delete', [ProjectController::class, 'destroy'])->name('project.delete');


        Route::group(['prefix' => 'project/{project}'], function () {
            Route::get('/', [ProjectController::class, 'show'])->name('project.show');
            Route::get('edit', [ProjectController::class, 'edit'])->name('project.edit');

            Route::delete('/', [ProjectController::class, 'destroy'])->name('project.destroy');
            Route::patch('update/', [ProjectController::class, 'update'])->name('project.update');

            // Delete
            //Route::get('delete', [ClientStatusController::class, 'delete'])->name('client.delete-permanently');
            // Restore
            //Route::get('restore', [ClientStatusController::class, 'restore'])->name('client.restore');

        });



    });
    
    /*
     * Tasks Management
     */
    Route::group(['namespace' => 'Task'], function () {


        /*
         * Tasks (only for System Type) CRUD
         */
        Route::get('task', [TaskController::class, 'index'])->name('task.index');
        Route::get('task/create', [TaskController::class, 'create'])->name('task.create');
        Route::post('task', [TaskController::class, 'store'])->name('task.store');
        Route::delete('task/delete', [TaskController::class, 'destroy'])->name('task.delete');

        Route::group(['prefix' => 'task/{task}'], function () {
            Route::get('/', [TaskController::class, 'show'])->name('task.show');
            Route::get('edit', [TaskController::class, 'edit'])->name('task.edit');

            Route::delete('/', [TaskController::class, 'destroy'])->name('task.destroy');
            Route::patch('update/', [TaskController::class, 'update'])->name('task.update');

            // Delete
            //Route::get('delete', [ClientStatusController::class, 'delete'])->name('client.delete-permanently');
            // Restore
            //Route::get('restore', [ClientStatusController::class, 'restore'])->name('client.restore');

        });
        
    });




});